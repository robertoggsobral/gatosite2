from django.urls import path

from . import views

app_name = 'gatos'
urlpatterns = [
    path('', views.GatoListView.as_view(), name='index'),
    path('create/', views.GatoCreateView.as_view(), name='create'),
    path('<int:pk>/', views.GatoDetailView.as_view(), name='detail'),
    path('update/<int:pk>/', views.GatoUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', views.GatoDeleteView.as_view(), name='delete'),
    path('<int:post_id>/comment/', views.create_comment, name='comment'),
    path('category/', views.CategoryListView.as_view(), name='category'),
    path('category/<int:pk>/', views.SinglecategoryDetailView.as_view(), name='singlecategory'),
]