from django.db import models
from django.conf import settings
from datetime import datetime  
from django.urls import reverse


class Post(models.Model):
    name = models.CharField(max_length=255)
    caracteristica = models.CharField(max_length=255)
    imagem = models.URLField(max_length=200, null=True)
    date = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return f'{self.name}'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    date = models.DateTimeField(default=datetime.now, blank=True)
    
    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    posts = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name}'