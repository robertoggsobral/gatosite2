from django import forms
from django.forms import ModelForm
from .models import Post, Comment


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'name',
            'caracteristica',
            'imagem',
        ]
        labels = {
            'name': 'Título',
            'caracteristica': 'Caracteristica',
            'imagem': 'URL da imagem',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Comentário',
        }