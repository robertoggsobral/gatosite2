from django.shortcuts import render
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from .models import Post, Comment, Category
from .forms import PostForm, CommentForm

class GatoDetailView(generic.DetailView):
    model = Post
    template_name = 'gatos/detail.html'


class GatoListView(generic.ListView):
    model = Post
    template_name = 'gatos/index.html'


class GatoCreateView(generic.CreateView):
    model = Post
    fields = ['name', 'caracteristica', 'imagem']
    template_name = 'gatos/create.html'
    success_url = '/gatos'


class GatoUpdateView(generic.UpdateView):
    model = Post
    fields = ['name', 'caracteristica', 'imagem']
    template_name = 'gatos/update.html'
    success_url = '/gatos'


class GatoDeleteView(generic.DeleteView):
    model = Post
    template_name = 'gatos/delete.html'
    success_url = '/gatos'


def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('gatos:detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'gatos/comment.html', context)


class CategoryListView(generic.ListView):
    model = Category
    template_name = 'gatos/category.html'


class SinglecategoryDetailView(generic.DetailView):
    model = Category
    template_name = 'gatos/singlecategory.html'