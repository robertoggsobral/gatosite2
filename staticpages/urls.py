from django.urls import path

from . import views

urlpatterns = [
    path('about/', views.about, name='about'),
    path('fotos/', views.fotos, name='fotos'),
    path('', views.index, name='index'),
]